﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

	public Text textComponentX;
	public Text textComponentY;

	// Use this for initialization
	void Start () {
		GameData.SetSizeX(5);
		GameData.SetSizeY(5);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetFieldSizeX(float x) {
		GameData.SetSizeX((int)x);
		textComponentX.text = x.ToString();
	}

	public void SetFieldSizeY(float y) {
		GameData.SetSizeY((int)y);
		textComponentY.text = y.ToString();
	}

	public void SwitchToScene(string sceneName) {
		GameData.InitUnitData();
		SceneManager.LoadScene(sceneName);
	}
}
