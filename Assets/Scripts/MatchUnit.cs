﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class MatchUnit : MonoBehaviour {

	private GameObject selection;
	private GameView gameView;
	private int x,y;

	// Use this for initialization
	void Start () {
	}

	public void SetSprite(string textureName) {
		GetComponent<SpriteRenderer>().sprite = StaticSprites.GetSprite(textureName);
	}

	public void SetUnitData(int x, int y) {
		this.x = x;
		this.y = y;
		this.RedrawUnit();
	}

	public void SetSelectionObject(GameObject selectionObject) {
		this.selection = selectionObject;
	}

	public void SetGameControaller(GameView gc) {
		this.gameView = gc;
	}

	void OnMouseUp(){
		SelectionController sc = selection.GetComponent<SelectionController>();

		if(!selection.activeSelf) {
			selection.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 93f);
			selection.SetActive(true);
			sc.SetCurrentSelection(this);
		} else {
			selection.SetActive(false);

			MatchUnit selectedUnit = sc.GetCurrentSelection();

			if(GameData.TryToSwapUnits(selectedUnit.x, selectedUnit.y, x, y)) {
				gameView.RedrawView();
			}

		}
	}

	public void RedrawUnit() {
		this.SetSprite((string)GameData.GetUnitData()[x,y].type.ToString());
	}
#if UNITY_EDITOR
	void OnDrawGizmos() 
	{
		GUIStyle style = new GUIStyle();
		style.normal.textColor = Color.green;

		Vector3 pos = new Vector3(transform.position.x - 0.3f, transform.position.y + 0.3f, transform.position.z);
		Handles.Label(pos, ((UnitDebugType)(int)GameData.GetUnitData()[x,y].type).ToString());

		pos = new Vector3(transform.position.x, transform.position.y + 0.3f, transform.position.z);
		Handles.Label(pos, ((int)GameData.GetUnitData()[x,y].horisontalWeight).ToString());

		pos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
		Handles.Label(pos, ((int)GameData.GetUnitData()[x,y].verticalWeight).ToString());
	}
#endif
}
