﻿using UnityEngine;

//Save my master!

public enum UnitType {empty, bitcoin, ethereum, litecoin, monero, ripple, zcash, dash}
public enum UnitDebugType {em, b, et, l, m, r, z, d}

public struct UnitData {
	public UnitType type;
	public int horisontalWeight;
	public int verticalWeight;
}

public static class GameData {
	private static int sizeX = 10, sizeY = 10;
	private static UnitData[,] unitData;

	public static void SetSizeX(int x) {
		sizeX = x;
	}

	public static void SetSizeY(int y) {
		sizeY = y;
	}

	public static int GetSizeX() {
		return sizeX;
	}

	public static int GetSizeY() {
		return sizeY;
	}

	public static UnitData[,] GetUnitData() {
		if(unitData == null) {
			InitUnitData();
		}

		return unitData;
	}

	//=============================================
	// Initialization
	//=============================================

	public static void InitUnitData() {
		unitData = new UnitData[sizeX, sizeY];
		for(int x = 0; x < sizeX; x++) {
			for(int y = 0; y < sizeY; y++) {
				//Try generate unit without match accure
				while(true) {
					if(InitUnit(x, y)) {
						break;
					}
				}
			}
		}
	}

	private static bool InitUnit(int x, int y) {
		unitData[x,y].horisontalWeight = 1;
		unitData[x,y].verticalWeight = 1;
		unitData[x,y].type = (UnitType)Random.Range(1, 7);

		//Check nearest groups for match
		if(x - 1 >= 0 && unitData[x-1, y].type == unitData[x,y].type){
			unitData[x,y].horisontalWeight += unitData[x-1, y].horisontalWeight;
		}
		if(y - 1 >= 0 && unitData[x, y-1].type == unitData[x,y].type){
			unitData[x,y].verticalWeight += unitData[x, y-1].verticalWeight;
		}

		if(unitData[x,y].horisontalWeight > 2 || unitData[x,y].verticalWeight > 2) {
			return false;
		}

		//Rise weight for same type neighbors
		if(x - 1 >= 0 && unitData[x-1, y].type == unitData[x,y].type) {
			unitData[x-1, y].horisontalWeight = unitData[x,y].horisontalWeight;
		}
		if(y - 1 >= 0 && unitData[x, y-1].type == unitData[x,y].type) {
			unitData[x, y-1].verticalWeight = unitData[x,y].verticalWeight;
		}

		return true;
	}

	//=============================================
	// Swap units
	//=============================================

	public static bool TryToSwapUnits(int x1, int y1, int x2, int y2) {
		//Check same type units
		if(unitData[x1,y1].type == unitData[x2,y2].type) {
			return false;
		}

		//Check unit distance
		if(Mathf.Abs(x1-x2) + Mathf.Abs(y1-y2) != 1) {
			return false;
		}

		//Try to swap units
		bool swap = false;
		if(x1 != x2) {
			swap = TryToSwapUnitsHorizontally(x1, y1, x2, y2);
		} else {
			swap = TryToSwapUnitsVertically(x1, y1, x2, y2);
		}

		//Try to remove matched units
		bool unitRemoved = false;
		if(swap) {
			unitRemoved = TryToRemoveUnit(x1, y1);
			unitRemoved = TryToRemoveUnit(x2, y2) || unitRemoved;
		}

		//Refill data after remove
		if(unitRemoved) {
			RefillData();
			//If we have matches after refill, remove them and repeat check
			while(RecalculateWeights()) {
				RemoveBestMatch();
				RefillData();
			}
		}

		return unitRemoved;
	}

	public static bool TryToSwapUnitsHorizontally(int x1, int y1, int x2, int y2) {
		SwapUnitsHorizontally(x1, y1, x2, y2);

		//Check on match
		if(unitData[x1,y1].horisontalWeight > 2 || unitData[x1,y1].verticalWeight > 2
			|| unitData[x2,y2].horisontalWeight > 2 || unitData[x2,y2].verticalWeight > 2) {
			FixNewPosition(x1, y1);
			FixNewPosition(x2, y2);
			return true;
		}

		//Swap back on no match
		SwapUnitsHorizontally(x1, y1, x2, y2);
		FixNewPosition(x1, y1);
		FixNewPosition(x2, y2);

		return false;
	}

	public static bool TryToSwapUnitsVertically(int x1, int y1, int x2, int y2) {
		SwapUnitsVertically(x1, y1, x2, y2);

		//Check on match
		if(unitData[x1,y1].horisontalWeight > 2 || unitData[x1,y1].verticalWeight > 2
			|| unitData[x2,y2].horisontalWeight > 2 || unitData[x2,y2].verticalWeight > 2) {
			FixNewPosition(x1, y1);
			FixNewPosition(x2, y2);
			return true;
		}

		//Swap back on no match
		SwapUnitsVertically(x1, y1, x2, y2);
		FixNewPosition(x1, y1);
		FixNewPosition(x2, y2);
		return false;
	}

	public static void SwapUnitsHorizontally(int x1, int y1, int x2, int y2) {
		//Decrease weight of left neighbors
		DecreaseNearWeights(x1,y1);
		unitData[x1,y1].horisontalWeight = 1;
		unitData[x1,y1].verticalWeight = 1;

		//Decrease weight of right neighbors
		DecreaseNearWeights(x2,y1);
		unitData[x2,y1].horisontalWeight = 1;
		unitData[x2,y1].verticalWeight = 1;

		//Swap units
		unitData[x1,y1].type = (UnitType)((int)unitData[x1,y1].type + (int)unitData[x2,y1].type);
		unitData[x2,y1].type = (UnitType)((int)unitData[x1,y1].type - (int)unitData[x2,y1].type);
		unitData[x1,y1].type = (UnitType)((int)unitData[x1,y1].type - (int)unitData[x2,y1].type);

		CalculateWeight(x1,y1);
		CalculateWeight(x2,y1);

		return;
	}

	public static void SwapUnitsVertically(int x1, int y1, int x2, int y2) {
		//Decrease weight of down neighbors
		DecreaseNearWeights(x1,y1);
		unitData[x1,y1].horisontalWeight = 1;
		unitData[x1,y1].verticalWeight = 1;


		//Decrease weight of up neighbors
		DecreaseNearWeights(x2,y2);
		unitData[x2,y2].horisontalWeight = 1;
		unitData[x2,y2].verticalWeight = 1;

		//Swap units
		unitData[x1,y1].type = (UnitType)((int)unitData[x1,y1].type + (int)unitData[x2,y2].type);
		unitData[x2,y2].type = (UnitType)((int)unitData[x1,y1].type - (int)unitData[x2,y2].type);
		unitData[x1,y1].type = (UnitType)((int)unitData[x1,y1].type - (int)unitData[x2,y2].type);

		CalculateWeight(x1,y1);
		CalculateWeight(x2,y2);

		return;
	}

	//Clean neighbors
	private static void DecreaseNearWeights(int x, int y) {
		if(x-1 >= 0 && unitData[x,y].type == unitData[x-1,y].type) {
			unitData[x-1,y].horisontalWeight = 1;
		}
		if(x+1 < sizeX && unitData[x,y].type == unitData[x+1,y].type) {
			unitData[x+1,y].horisontalWeight = 1;
		}
		if(y-1 >= 0 && unitData[x,y].type == unitData[x,y-1].type) {
			unitData[x,y-1].verticalWeight = 1;
		}
		if(y+1 < sizeY && unitData[x,y].type == unitData[x,y+1].type) {
			unitData[x,y+1].horisontalWeight = 1;
		}
	}

	//Match check
	private static void CalculateWeight(int x, int y){
		if(x - 1 >= 0 && unitData[x-1, y].type == unitData[x,y].type){
			unitData[x,y].horisontalWeight += unitData[x-1, y].horisontalWeight;
		}
		if(x + 1 < sizeX && unitData[x+1, y].type == unitData[x,y].type){
			unitData[x,y].horisontalWeight += unitData[x+1, y].horisontalWeight;
		}
		if(y - 1 >= 0 && unitData[x, y-1].type == unitData[x,y].type){
			unitData[x,y].verticalWeight += unitData[x, y-1].verticalWeight;
		}
		if(y + 1 < sizeY && unitData[x, y+1].type == unitData[x,y].type){
			unitData[x,y].verticalWeight += unitData[x, y+1].verticalWeight;
		}
	}

	//On match update neighbors with new weight
	private static void FixNewPosition(int x, int y) {
		int left = 1;
		int right = 1;
		int up = 1;
		int down = 1;

		UnitType checkType = unitData[x,y].type;

		while(x-left >= 0 && checkType == unitData[x-left, y].type) {
			unitData[x-left,y].horisontalWeight = unitData[x,y].horisontalWeight;
			left++;
		}

		while(x+right < sizeX && checkType == unitData[x+right, y].type) {
			unitData[x+right,y].horisontalWeight = unitData[x,y].horisontalWeight;
			right++;
		}

		while(y-down >= 0 && checkType == unitData[x, y-down].type) {
			unitData[x,y-down].verticalWeight = unitData[x,y].verticalWeight;
			down++;
		}

		while(y+up < sizeY && checkType == unitData[x, y+up].type) {
			unitData[x,y+up].verticalWeight = unitData[x,y].verticalWeight;
			up++;
		}
	}

	//=============================================
	// Remove units
	//=============================================
	public static bool TryToRemoveUnit(int x, int y) {
		bool removeUnit = false;
		//Remove horisontal match
		if(unitData[x,y].horisontalWeight > 2) {
			RemoveUnitsHorisontallyFrom(x, y);
			removeUnit = true;
		}
		//Remove vertical match
		if(unitData[x,y].verticalWeight > 2) {
			RemoveUnitsVerticallyFrom(x, y);
			removeUnit = true;
		}
		//Remove central match unit
		if(removeUnit) {
			RemoveUnit(x,y);
		}

		return removeUnit;
	}

	private static void RemoveUnitsHorisontallyFrom(int x, int y) {
		int left = 1;
		int right = 1;

		UnitType checkType = unitData[x,y].type;

		//Remove sub match unit and clean neighbors weidhts
		while(x-left >= 0 && checkType == unitData[x-left, y].type) {
			DecreaseNearWeights(x-left,y);
			RemoveUnit(x-left,y);
			left++;
		}

		//Remove sub match unit and clean neighbors weidhts
		while(x+right < sizeX && checkType == unitData[x+right, y].type) {
			DecreaseNearWeights(x+right,y);
			RemoveUnit(x+right,y);
			right++;
		}
	}

	private static void RemoveUnitsVerticallyFrom(int x, int y) {
		int up = 1;
		int down = 1;

		UnitType checkType = unitData[x,y].type;

		//Remove sub match unit and clean neighbors weidhts
		while(y-down >= 0 && checkType == unitData[x, y-down].type) {
			DecreaseNearWeights(x,y-down);
			RemoveUnit(x,y-down);
			down++;
		}

		//Remove sub match unit and clean neighbors weidhts
		while(y+up < sizeY && checkType == unitData[x, y+up].type) {
			DecreaseNearWeights(x,y+up);
			RemoveUnit(x,y+up);
			up++;
		}
	}

	private static void RemoveUnit(int x, int y) {
		unitData[x,y].horisontalWeight = 0;
		unitData[x,y].verticalWeight = 0;
		unitData[x,y].type = UnitType.empty;
	}

	private static void RefillData() {
		for(int x=0; x < sizeX; x++) {
			for(int y=0; y < sizeY; y++) {
				//Search empty from bottom to top
				if(unitData[x,y].type == UnitType.empty) {
					UnitType mtype = UnitType.empty;
					//Search next not empty
					for(int my=y; my < sizeY; my++) {
						mtype = unitData[x,my].type;
						if(mtype != UnitType.empty) {
							//Clear old position
							RemoveUnit(x,my);
							break;
						}
					}
					//Generate new unit if there are no upper units
					if(mtype == UnitType.empty) {
						unitData[x,y].type = (UnitType)Random.Range(1, 7);
					} else {
						unitData[x,y].type = mtype;
					}
				}
			}
		}
		return;
	}

	private static bool RecalculateWeights() {
		bool hasMatches = false;
		//Recalculate vertical weights
		for(int x=0; x < sizeX; x++) {
			UnitType curType = unitData[x,0].type;
			int curVerticalWeight = 0;
			for(int y=0; y < sizeY; y++) {
				//Increase till max vertical weight
				if(curType == unitData[x,y].type) {
					curVerticalWeight++;
				} else {
					//Fill lower units with max weight
					for(int b = curVerticalWeight; b > 0 ; b--) {
						unitData[x,y-b].verticalWeight = curVerticalWeight;
						// Also check matches
						if(curVerticalWeight > 2) {
							hasMatches = true;
						}
					}
					curType = unitData[x,y].type;
					curVerticalWeight = 1;
				}
			}
			//Recalculate top group
			for(int b = curVerticalWeight; b > 0 ; b--) {
				unitData[x,sizeY-b].verticalWeight = curVerticalWeight;
				// Also check matches
				if(curVerticalWeight > 2) {
					hasMatches = true;
				}
			}
		}

		for(int y=0; y < sizeY; y++) {
			UnitType curType = unitData[0,y].type;
			int curHorisontalWeight = 0;
			for(int x=0; x < sizeX; x++) {
				//Increase till max horisontal weight
				if(curType == unitData[x,y].type) {
					curHorisontalWeight++;
				} else {
					//Fill left units with max weight
					for(int b = curHorisontalWeight; b > 0 ; b--) {
						unitData[x-b,y].horisontalWeight = curHorisontalWeight;
						// Also check matches
						if(curHorisontalWeight > 2) {
							hasMatches = true;
						}
					}
					curType = unitData[x,y].type;
					curHorisontalWeight = 1;
				}
			}
			//Recalculate right group
			for(int b = curHorisontalWeight; b > 0 ; b--) {
				unitData[sizeX-b,y].horisontalWeight = curHorisontalWeight;
				// Also check matches
				if(curHorisontalWeight > 2) {
					hasMatches = true;
				}
			}
		}

		return hasMatches;
	}

	private static void RemoveBestMatch() {
		int bestMatchX = 0;
		int bestMatchY = 0;
		int bestMatchVal = 0;
		//Search unit with best weights 
		for(int x=0; x < sizeX; x++) {
			for(int y=0; y < sizeY; y++) {
				if(unitData[x,y].horisontalWeight > 2 || unitData[x,y].verticalWeight > 2) {
					if(unitData[x,y].horisontalWeight + unitData[x,y].verticalWeight > bestMatchVal) {
						bestMatchVal = unitData[x,y].horisontalWeight + unitData[x,y].verticalWeight;
						bestMatchX = x;
						bestMatchY = y;
					}
				}
			}
		}
		//Remove best weight unit
		if(bestMatchVal > 0) {
			TryToRemoveUnit(bestMatchX, bestMatchY);
		}
	}
}
