﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticSprites : MonoBehaviour {

	public static Dictionary<string, Sprite> dictSprites = new Dictionary<string, Sprite>();

	// Use this for initialization
	void Start () {
		LoadUnitResources();
	}

	public static void LoadUnitResources() {
		if(dictSprites.Count > 0) {
			return;
		}

		Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites");
		for(int i=0; i< sprites.Length; i++) {
			dictSprites.Add(sprites[i].name, sprites[i]);
		}
	}

	public static Sprite GetSprite(string spriteName) {
		if(dictSprites.Count == 0) {
			LoadUnitResources();
		}
		return dictSprites[spriteName];
	}
}
