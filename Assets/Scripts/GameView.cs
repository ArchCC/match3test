﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameView : MonoBehaviour {

	public GameObject background;
	public GameObject unitPrefab;

	public GameObject selectionSprite;

	private static float realUnitSize = 0.75f;

	private MatchUnit[,] units;

	// Use this for initialization
	void Start () {
		SpriteRenderer renderer = background.GetComponent<SpriteRenderer>();
		renderer.size = new Vector2((float)(realUnitSize * GameData.GetSizeX()), (float)(realUnitSize * GameData.GetSizeY()));

		float currentX = 0 - realUnitSize * GameData.GetSizeX() / 2 + realUnitSize / 2;
		float currentY = 0 - realUnitSize * GameData.GetSizeY() / 2 + realUnitSize / 2;
		float beginY = currentY;

		units = new MatchUnit[GameData.GetSizeX(), GameData.GetSizeY()];

		for(int x = 0; x < GameData.GetSizeX(); x++) {
			for(int y = 0; y < GameData.GetSizeY(); y++) {
				GameObject unit = (GameObject)Instantiate(unitPrefab, new Vector3(currentX, currentY, 90f), Quaternion.identity);
				MatchUnit matchUnit = unit.GetComponent<MatchUnit>();
				matchUnit.SetUnitData(x,y);
				matchUnit.SetSelectionObject(selectionSprite);
				matchUnit.SetGameControaller(this);

				units[x,y] = matchUnit;
				currentY += realUnitSize;
			}
			currentX += realUnitSize;
			currentY = beginY;
		}
	}

	public void RedrawUnitsColumn(int x) {
		for(int y = 0 ; y < GameData.GetSizeY(); y++) {
			units[x,y].RedrawUnit();
		}
	}

	public void RedrawUnitsRow(int y) {
		for(int x = 0 ; x < GameData.GetSizeX(); x++) {
			units[x,y].RedrawUnit();
		}
	}

	public void RedrawView() {
		for(int x = 0 ; x < GameData.GetSizeX(); x++) {
			for(int y = 0 ; y < GameData.GetSizeY(); y++) {
				units[x,y].RedrawUnit();
			}
		}
	}
}
