﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionController : MonoBehaviour {

	private MatchUnit selectedObject;

	public void SetCurrentSelection(MatchUnit selectedObject) {
		this.selectedObject = selectedObject;
	}

	public MatchUnit GetCurrentSelection() {
		return this.selectedObject;
	}
}
